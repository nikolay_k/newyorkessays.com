/**
 * Author: Nikolay Kovalenko
 * Date: 12.09.2017
 * Email: nikolay.arkadjevi4@gmail.com
 * */

var newYork = {
    header: '.navbar',
    menuWrapper : '.header',
    menuList : 'ul.menu',
    menuitem: 'menuttoggle',
    footer: '.newYork__footer',
    banner: '.bottom-fixed-banner',
    contactForm: '#contactForm',
    windows: window,
    chat: '#chat',
    socials: '#shares',
    curDevice: window.Detectizr.device.type,
    openSearch: false,


    init: function () {
        this.headerMenu(this.menuWrapper, this.menuList);
        this.chatAnimate(this.chat);
        //this.bannerScroll(this.banner);
        //this.bannerClose(this.banner);
    },

    headerMenu: function (header, menu) {
        $this = this;

        var windows = $(this.windows);

       if (this.curDevice === "tablet"){
            console.log("tru");
            $('.has-dropdown > a').click(function (e) {
                e.preventDefault();
                e.stopPropagation();
            });
        }

        function initDropdown() {
            $(menu).find("li").each(function() {
                if ($(this).children("ul.dropdown").length > 0){
                    $(this).children("ul.dropdown").addClass("submenu");
                    $(this).children("a").append("<span class='indicator'><i class='fa fa-angle-down'></i></span>");
                }
            })
        }

        function addOverlay() {
            $('body').append("<div class='overlay'></div>");

            $('body').find(".overlay").fadeIn(300).on("click touchstart", function(o) {
                closeMenu();
            });

            $('body').find(".menutoggle-close").on("click touchstart", function(o) {
                o.preventDefault();
                closeMenu();
                $(this).removeClass('open');
                $('.menutoggle').removeClass('open');
            })
        }

        function removeOverlay() {
            $('body').find(".overlay").fadeOut(400, function() {
                $(this).remove();
            })
        }

        function openMenu() {
            $(header).on("click touchstart", "a.close-icon-wrap", function(event) {
                event.stopPropagation();
                event.preventDefault();
                closeMenu();
            });
            $(header).find("ul.menu").addClass("open");
            addOverlay();
        }

        function closeMenu() {
            $(header).find("ul.menu").removeClass("open");
            removeOverlay();
        }

        function initEvent() {
            $(menu).off("mouseenter mouseleave")
        }

        function openDropdown() {
            initEvent();
            $(menu).on("mouseenter mouseleave", "li", function() {
                $(this).children("ul.dropdown").stop(!0, !1).fadeToggle(150)
            })
        }

        function subMenu() {
            initEvent();
            $(menu).find("ul.submenu").hide(0);
            $(menu).find(".indicator").removeClass("indicator-up");
            $(menu).on("click", ".indicator", function(event) {
                return event.stopPropagation(), event.preventDefault(), $(this).parent("a").parent("li").siblings("li").find(".submenu").stop(!0, !0).delay(300).slideUp(300), $(this).closest(".nav-menu").siblings(".nav-menu").children("li").parent("li").siblings("li").find("ul.submenu").stop(!0, !0).delay(300).slideUp(300), "none" == $(this).parent("a").siblings(".submenu").css("display") ? ($(this).addClass("indicator-up"), $(this).parent("a").parent("li").siblings("li").find(".indicator").removeClass("indicator-up"), $(this).closest("ul.menu").siblings("ul.menu").find(".indicator").removeClass("indicator-up"), $(this).parent("a").parent("li").children(".submenu").stop(!0, !0).delay(300).slideDown(300), !1) : ($(this).parent("a").parent("li").find(".indicator").removeClass("indicator-up"), void $(this).parent("a").parent("li").find(".submenu").stop(!0, !0).delay(300).slideUp(300))
            });
        }

        function initMenu() {
            window.innerWidth < 768 ? ($(header).addClass("mobile"), subMenu()) : ($(header).removeClass("mobile"), openDropdown(), removeOverlay(),closeMenu())
        }

        $(header).on("click touchstart", ".menutoggle", function(event) {
            event.stopPropagation();
            event.preventDefault();
            openMenu();
            $(this).toggleClass('open');
            $('.menutoggle-close').toggleClass('open');
        });

        initDropdown(); initMenu();

        windows.resize(function() {
            initMenu();
        });

    },


    chatAnimate: function (chat) {
        // find end of animation css
        function whichAnimationEvent() {
            var t,
                el = document.createElement("fakeelement");

            var animations = {
                "animation": "animationend",
                "OAnimation": "oAnimationEnd",
                "MozAnimation": "animationend",
                "WebkitAnimation": "webkitAnimationEnd"
            }

            for (t in animations) {
                if (el.style[t] !== undefined) {
                    return animations[t];
                }
            }
        }
        var animationEvent = whichAnimationEvent();

        // main
        $(chat).one(animationEvent, function(event) {
        });

        $(chat).find('.chat__close').click(function() {
            $(chat).fadeOut();
        });
    },

    bannerScroll: function (banner) {
        if ($(banner).offset().top + $(banner).height() >= $(this.footer).offset().top - 10) {
            $(banner).css({
                'position': 'absolute',
                'top': '-60px'
            });
        }
        if (window.matchMedia('(max-width: 768px)').matches) {
            $('.bottom-fixed-banner').css({
                'position': 'absolute',
                'top': '-60px'
            });
        }

        if ($(document).scrollTop() + window.innerHeight < $(this.footer).offset().top) {
            $(banner).css({
                'position': 'fixed',
                'top': 'auto'
            });
        }
    },

    bannerClose: function (banner) {
        $('#banner-close').click(function () {
            $(banner).fadeOut();
        });
    },

    flashcard: function () {
        $('.first-col').each(function(){
            if ($(this).html() !== "") {
                $('.first-col').addClass('not-empty');
                $('.phd-flex-item').addClass('width-33');
            }
        });
    },

};

$(document).ready(
    function () {
        newYork.init();
        SmoothScroll({
            stepSize: 80
        });

        document.querySelector('.menutoggle').onclick = function() {
            newYork.checkState(this);
            console.log("dd");
        }
    }
);

$(window).scroll(
    function () {
        newYork.scrollHeader(newYork.header);
        if ($(newYork.banner).length > 0) {
            newYork.bannerScroll(newYork.banner);
        }
    }
);

$(window).resize(function() {

});

